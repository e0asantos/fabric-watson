'use strict';

var watson = require('watson-developer-cloud'),
fs         = require('fs'),
Gpio = require('onoff').Gpio,
button = new Gpio(4, 'in', 'both'),
path       = require('path'),
extend     = require('util')._extend,
recorder = require('node-record-lpcm16');
var randomstring = require("randomstring");
var sys = require('sys')
var eos = require('end-of-stream');
var play = require('./play').Play();
var currentSoundFile="";
var waitingForSensor=false;
var dialog = watson.dialog({
  username: 'cafa263e-4bec-46cb-8d21-6374b3cb83cd',
  password: '6aFoQgmEqqiX',
  version: 'v1'
});
var language_translation = watson.language_translation({
  username: 'be0bc621-1d0d-44f2-bed4-4c03759fb43f',
  password: 'a3xrcYkdlTNp',
  version: 'v2'
});
var text_to_speech = watson.text_to_speech({
  username: '448023d6-e7df-4769-90db-7729038a50fc',
  password: 'wZ4OAXCHr9jj',
  version: 'v1',
  url: 'https://stream.watsonplatform.net/text-to-speech/api'
});
var paramsTTS = {
  text: 'Hello from IBM Watson',
  voice: 'es-US_SofiaVoice',//'en-US_AllisonVoice', // Optional voice
  accept: 'audio/wav'
};
//text_to_speech.synthesize(paramsTTS).pipe(fs.createWriteStream('output.wav'));
// record.on("recordingEnded",function(){
//   console.log("end.....");
// })
var readline = require('readline'),
rl = readline.createInterface(process.stdin, process.stdout);
var speech_to_text = watson.speech_to_text({
  username: 'b0d3883a-ab02-45be-8813-39099b04dce5',
  password: '9PS7wjwOhvEV',
  version: 'v1',
  url: 'https://stream.watsonplatform.net/speech-to-text/api'
});
var globalRecording=null;
//upload dialogs
var paramsSpeech = {
  content_type: 'audio/wav',
  model:'es-ES_BroadbandModel'
};
// create the stream
var recognizeStream = speech_to_text.createRecognizeStream(paramsSpeech);
function puts(error, stdout, stderr) {sys.puts(stdout)}

['data', 'results', 'error', 'connection-close'].forEach(function(eventName) {
  //recognizeStream.on(eventName, console.log.bind(console, eventName + ' event: '));
  
});

dialog.getDialogs({}, function (err, dialogs) {
    if (err){
      console.log('error:', err);
    } 
    else {
      //console.log(JSON.stringify(dialogs, null, 2));
    }
});
var conversation_id=null;
var client_id=null;

var params = extend({ dialog_id: "20db6f97-7f6d-4700-97a3-a58c156ad765" }, "hello");
dialog.conversation(params, function(err, results) {
if (err)
  return next(err);
else
  //console.log(results);
  console.log(results.client_id);
  conversation_id=results.conversation_id;
  console.log(results.conversation_id);
  client_id=results.client_id;
  watsonSpeak(results.response[0])
  
  
  // eos(globalRecording, function(err) {
  //   if (err) return console.log('stream had an error or closed early');
  //   console.log('stream has finished');
  //   console.log("interpretando");
  //   fs.createReadStream(__dirname + '/'+currentSoundFile).pipe(recognizeStream);
  //   recognizeStream.setEncoding('utf8'); // to get strings instead of Buffers from `data` events
  // });
  
});
function watsonSpeak(statement){
  language_translation.translate({
      text: statement, source : 'en', target: 'es' },
      function (err, translation) {
        if (err)
          console.log('error:', err);
        else
          paramsTTS.text=translation.translations[0].translation;
          var saveWav=fs.createWriteStream('output.wav');
          eos(saveWav, function(err) {
            if (err) return console.log('stream had an error or closed early');
            play.sound('output.wav',function(){
              makeQuestion(statement);
              console.log("grabando");
              currentSoundFile=randomstring.generate(7)+".wav";
              globalRecording = fs.createWriteStream(currentSoundFile, { encoding: 'binary' });
              recorder.start({
                sampleRate : 44100,
                verbose : true,
                threshold  : 2,
                onEnd:onMyEnd,
                onEmptyResponse:emptyResponse
              }).pipe(globalRecording);
            });
            
          });
          text_to_speech.synthesize(paramsTTS).pipe(saveWav);
        })
}
function emptyResponse(){
  waitingForSensor=true;
  console.log("Waiting for sensor to activate")
}
function makeQuestion(question){
	rl.question(question, function(answer) {
	  //console.log('Oh, so your favorite food is ' + answer);
	  console.log("A:"+answer);
	  var params = { dialog_id: "20db6f97-7f6d-4700-97a3-a58c156ad765",conversation_id:conversation_id,client_id:client_id,input:answer };
		dialog.conversation(params, function(err, results) {
		if (err)
		  console.log(err);
		else
		  console.log(results);
		for (var i = results.response.length - 1; i >= 0; i--) {
			if (results.response[i]!="") {
        var saveWav=fs.createWriteStream('output.wav');
				makeQuestion(results.response[i]);
				return;
			};
			
		};
		
		});
	});
}
function onMyEnd(){
  console.log("interpretando");
  recognizeStream = speech_to_text.createRecognizeStream(paramsSpeech);
  recognizeStream.on('error',function(dato){
    console.log("interpreting error")
  });
  recognizeStream.on('results',function(dato){
    console.log("interpreting results");
    waitingForSensor=true;
  });
  recognizeStream.on('connection-close',function(dato){
    console.log("interpreting close")
  });
  recognizeStream.on('data',function(dato){
    console.log("text=> "+dato);
    language_translation.translate({
      text: dato, source : 'es', target: 'en' },
      function (err, translation) {
        if (err)
          console.log('error:', err);
        else
          //console.log(JSON.stringify(translation, null, 2));
          console.log("to en=>"+translation.translations[0].translation);
          makeVoiceQuestion(translation.translations[0].translation);
    });
  });
  fs.createReadStream(__dirname + '/'+currentSoundFile).pipe(recognizeStream);
  recognizeStream.setEncoding('utf8'); // to get strings instead of Buffers from `data` events
}
function translateInto(input,lang){
  var currentLang="";
  if (lang=="es") {

  }
}
function makeVoiceQuestion(answer){
  var params = { dialog_id: "20db6f97-7f6d-4700-97a3-a58c156ad765",conversation_id:conversation_id,client_id:client_id,input:answer };
    dialog.conversation(params, function(err, results) {
    if (err)
      console.log(err);
    else
      console.log(results);
    for (var i = results.response.length - 1; i >= 0; i--) {
      if (results.response[i]!="") {
        watsonSpeak(results.response[i]);
        //makeQuestion(results.response[i]);
        //currentSoundFile=randomstring.generate(7)+".wav";
        //globalRecording = fs.createWriteStream(currentSoundFile, { encoding: 'binary' });
        // recorder.start({
        //   sampleRate : 44100,
        //   verbose : true,
        //   onEnd:onMyEnd
        // }).pipe(globalRecording);
        // eos(globalRecording, function(err) {
        //   if (err) return console.log('stream had an error or closed early');
        //   console.log('stream has finished');
        //   console.log("interpretando");
        //   fs.createReadStream(__dirname + '/'+currentSoundFile).pipe(recognizeStream);
        //   recognizeStream.setEncoding('utf8'); // to get strings instead of Buffers from `data` events
        // });
        return;
      };
      
    };
    
    });
}
button.watch(function (err, value) {
  if (err) {
    throw err;
  }
  console.log("voice");
  if (waitingForSensor) {
    currentSoundFile=randomstring.generate(7)+".wav";
    globalRecording = fs.createWriteStream(currentSoundFile, { encoding: 'binary' });
    recorder.start({
      sampleRate : 44100,
      verbose : true,
      threshold  : 2,
      onEnd:onMyEnd,
      onEmptyResponse:emptyResponse
    }).pipe(globalRecording);
  } else {
    waitingForSensor=false;
  }
});
function exit() {
  console.log("quitting");
  button.unexport();
  process.exit();
}
process.on('SIGINT', exit);