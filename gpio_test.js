var Gpio = require('onoff').Gpio,
  led = new Gpio(14, 'out'),
  button = new Gpio(4, 'in', 'both');
console.log("init");
function exit() {
console.log("quitting");
  led.unexport();
  button.unexport();
  process.exit();
}

button.watch(function (err, value) {
  if (err) {
    throw err;
  }
console.log("button");
  led.writeSync(value);
});

process.on('SIGINT', exit);